const express = require('express')
const directorsrouter = require('../routers/directors')
const moviesrouter = require('../routers/movies')
const app = express()

app.get('/',(req, res) => {
    res.send("welcome to movies api");
})
//using middleware to handle request on directors 
app.use('/directors',directorsrouter)
//using middle ware to handle  request on movies
app.use('/movies',moviesrouter)

const port = process.env.PORT || 5001;
app.listen(port, () => console.log(`listening on port ${port}`));
const express =require('express')
const director = express.Router();
var bodyParser = require('body-parser')

const functions = require("../functions/directorsfunction.js")

director.use(bodyParser.json())

director.get('/',(req, res) => {
    const  result =  functions.getdirectors()
    result.then(result => res.status(200).send(result))
    .catch(e => {
        res.status(404).send({data:{error: 'cant receive data'}})
    })
})
director.get('/:directorid',(req, res) => {
    const  result =  functions.getdirectorsnames(parseInt(req.params.directorid));
    result.then(result => res.status(200).send(result))
    .catch(e => {
        res.status(404).send({data:{error: 'cant receive data'}})
    })
})
director.delete('/:directorid',(req, res) => {
    const  result =  functions.deletedirectorsname(parseInt(req.params.directorid));
    result.then(result => res.status(200).send(result))
    .catch(e => {
        res.status(404).send({data:{error: 'cant receive data'}})
    }) 
})
director.put('/:directorid',(req, res) => {
    const result = functions.updatedirectorsname(parseInt(req.params.directorid),req.body.name)
    result.then((result) => res.status(404).send(result))
    .catch(e => {
        res.status(404).send({data:{error: 'cant receive data'}})
    })
})
director.post('/',(req, res) => {
    console.log(req.body)
    const  result =  functions.insertdirectors(req.body);
    result.then(result => res.status(200).send(result))
    .catch(e => {
        res.status(404).send({data:{error: 'cant receive data'}})
    })
})
module.exports = director

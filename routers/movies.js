const express =require('express')
const movie = express.Router();
var bodyParser = require('body-parser')

const functions = require("../functions/moviesfunction.js")
movie.use(bodyParser.json())
//handling the get request on movie table
movie.get('/',(req, res) => {
    const  result =  functions.getallmovies()
    result.then(result => res.status(200).send(result))
    .catch(e => {
        res.status(404).send({data:{error: 'cant receive data'}})
    })
})
//handling the get request on movie table
movie.get('/:id',(req, res) => {
    const  result =  functions.getmoviesnames(parseInt(req.params.id));
    result.then(result => res.status(200).send(result))
    .catch(e => {
        res.status(404).send({data:{error: 'cant receive data'}})
    })
})
//handling the delete request on movie table
movie.delete('/:id',(req, res) => {
    const  result =  functions.deletemovie(parseInt(req.params.id));
    result.then(result => res.status(200).send(result))
    .catch(e => {
        res.status(404).send({data:{error: 'cant receive data'}})
    }) 
})
//handling the update request on the movie table 
movie.put('/:id',(req, res) => {
    const  result =  functions.updatemovie(req.params.id,req.body);
    result.then(result => res.status(200).send(result))
    .catch(e => {
        res.status(404).send({data:{error: 'cant receive data'}})
    })
})
//handling the post request on movie table
movie.post('/',(req, res) => {
    const  result =  functions.insertmovie(req.body);
    result.then(result => res.status(200).send(result))
    .catch(e => {
        res.status(404).send({data:{error: 'cant receive data'}})
    })
})
module.exports = movie 

var fs = require("fs");
var jsondirectors = require("../movies.json");
const { Client } = require('pg');

const client = new Client({

  user: "prasanna",

  host: "localhost",

  database: "directors",

  password: "prasanna@123",

  port: 5432

});
const directors=[]
    const direnames = jsondirectors.reduce((direnames,row) => {
       direnames[row['Director']] = (direnames[row['Director']] || 0) +1
       return direnames;
    },{});
    const directorsnames = Object.keys(direnames);
    const directorsarray = directorsnames.reduce((directorsarray,name) => {
        mediatorarray=[]
        mediatorarray.push(name);
        directorsarray.push(mediatorarray)
        return directorsarray; 
    },[]);

//connecting to the database
client.connect()
.then(()=> console.log("conection established successfully "))
//creating  the directors table 
function createdirectortable(){
    client.query(`CREATE TABLE IF NOT EXISTS directors(id SERIAL PRIMARY KEY NOT NULL,name text)`,(err, res) => {
        if (err){
        console.log(err)
        }
        else {
            console.log("directors table created successfully");
        }
        client.end()
    })
}
//creating the movie table
function createmovietable(){
   client.query(`CREATE TABLE IF NOT EXISTS movies(
        Rankid  INTEGER PRIMARY KEY NOT NULL, 
        Title VARCHAR(100)
        ,Description VARCHAR(300),
        Runtime INTEGER,
        Genre VARCHAR(50),
        Rating FLOAT,
       Metascore VARCHAR(100),
        Votes INTEGER,
       grosearning VARCHAR(50),
        id INTEGER references directors(id) ON DELETE CASCADE,
        Actor VARCHAR(50),Year NUMERIC`,(err, res) => {
            if (err){
                console.log(err)
                }
            else {
                console.log("directors table created successfully");
            }
                client.end()
            })
}
//inserting the data into directors table
function insertdirectorsdata(){
    const querry = 'INSERT INTO directors(name) VALUES($1)'
    directorsarray.forEach(row => {
    client.query(querry,row,(err, res)=> {
        console.log(err, res);    
    })
  })
}
//inserting the data to movie table
function insertmoviedata(){
    const movies = jsondirectors.reduce((movies,row) => {
        const moviedetails = []
        moviedetails.push(row['Rank'],row['Title'],row['Description'],row['Runtime'],row['Genre'],row['Rating'],row['Metascore'],row['Votes'],row['Gross_Earning_in_Mil'])
        x=row['Director']
        const y = directorsnames.findIndex(name => name ===  x)
        moviedetails.push(y+1)
    moviedetails.push(row['Actor'],row['Year'])
    movies.push(moviedetails);
    return movies;
    },[])
    const querry = `INSERT INTO movies(rankid,title,description,runtime,genre,rating,metascore,votes,grosearning,id,actor,year)
     VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12)`
    movies.forEach(row => {
    client.query(querry,row,(err, res)=> {
        console.log(err, res);  
    })
  })
}
module.exports = {createdirectortable,createmovietable,insertdirectorsdata,insertmoviedata}
const { Client } = require('pg');

const client = new Client({

  user: "prasanna",

  host: "localhost",

  database: "directors",

  password: "prasanna@123",

  port: 5432

});
//connecting to the database
client.connect()
.then(() => console.log("connected successfully"))
//returning all directors details
async function getdirectors(){
  query = `SELECT * FROM directors`
  const result =await  client.query(query)
  return result.rows;
}

//returning specific director details

async function getdirectorsnames(id){
  query = `SELECT name FROM directors WHERE id = ${id}`
  const result =await  client.query(query)
  return result.rows;
}

// deleteting directors name from database 

async function deletedirectorsname(id){
  query = `DELETE FROM directors WHERE id = ${id}`
  const result =  await client.query(query)
  return result;
}

// updating directorsdetails

async function updatedirectorsname(id,directorname){
  query = `UPDATE directors set name = '${directorname}' WHERE id = ${id}`
  const result =  await client.query(query)
  return result;
}

// inserting new director details  in the database 

async function insertdirectors(director){
  query = `INSERT INTO directors(name) VALUES ('${director.name}')`
  const result =await  client.query(query)
  return result;
}
module.exports = {getdirectors,getdirectorsnames,deletedirectorsname,updatedirectorsname,insertdirectors}
const { Client } = require('pg');

const client = new Client({

  user: "prasanna",

  host: "localhost",

  database: "directors",

  password: "prasanna@123",

  port: 5432

});
// connecting to the database 
client.connect()
.then(() => console.log("connected successfully"))
//returning all movie  details from movies table
async function getallmovies(){
    query = `SELECT * FROM movies`
    const result =await client.query(query)
    return result.rows;
  }
  //returning specific movie id deails 
  async function getmoviesnames(id){
    query = `SELECT * FROM movies WHERE rankid = ${id}`
    const result =await  client.query(query)
    return result.rows;
  }
  //deleting movie details
  async function deletemovie(id){
    query = `DELETE FROM movies WHERE id = ${id}`
    const result =  await client.query(query)
    return result;
  }
  //updatig movie details
  async function updatemovie(id,movie){
    query = `UPDATE movies set 
    title = '${movie.title}', description ='${movie.description}',runtime = ${movie.runtime},genre = '${movie.genre}',
    rating = ${movie.rating},metascore = ${movie.metascore},votes = ${movie.votes},grosearning = ${movie.grosearning},
    id = ${movie.id},actor = '${movie.actor}',year = ${movie.year}
     WHERE rankid = ${id}`
    const result =  await client.query(query)
    return result;
  }
  //inserting new data on movies table
  async function insertmovie(movie){
    query = `INSERT INTO 
    movies(rankid,title,description,runtime,genre,rating,metascore,votes,grosearning,id,actor,year) 
    VALUES (${movie.rankid},'${movie.title}','${movie.description}',${movie.runtime},'${movie.genre}',${movie.rating},${movie.metascore},${movie.votes},${movie.grosearning},${movie.id},'${movie.actor}',${movie.year})`
    const result =await  client.query(query)
    return result;
  }
module.exports = {getallmovies,getmoviesnames,deletemovie,updatemovie,insertmovie}  